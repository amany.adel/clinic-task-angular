import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'
import { User } from '../_model/user';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient) {
    //this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    //  this.currentUser = this.currentUserSubject.asObservable();
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }
  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }



  getUser(email, password): Observable<User> {
    return this.http.post<User>("http://localhost:8800/login", { email, password }).pipe(map(user => {

      //  login successful if there's a jwt token in the response
      if (user.token) {
        console.log("test login");
        console.log(user);

        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
      }

      return user;
    }))

  }
  logout():Observable<any> {
    // remove user from local storage to log user out
    return this.http.post<any>('http://localhost:8800/logOut');
    
  }




}
