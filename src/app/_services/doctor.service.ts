import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  private httpOptions: any;
  currentUser: any;
  publicTest: any;


  constructor(
    private http: HttpClient,
    private socket: Socket
    
  ) {
    console.log("in constructor of doctor services");

  }


  // Get history for doctor
  getHistoryDoctor(): Observable<any> {
    
    return this.http.post<any>('http://localhost:8800/doctor/allRequestforDoctor');
  }

  sendDoctorReport(requestId,reportDoctor): Observable<any> {
    
    return this.http.post<any>('http://localhost:8800/doctor/reportOnRequest'
    ,{requestId,reportDoctor});
  }

  fireGetRequestsDoctor(){
    this.socket.emit('allRequestForDoctor');
  }
  listenGetRequestsDoctor():Observable<any>
  {
    return this.socket
        .fromEvent("allRequestForDoctor");
  }
         
}
