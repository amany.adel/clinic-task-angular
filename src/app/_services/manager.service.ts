import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Socket } from 'ngx-socket-io';


@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  private httpOptions: any;
  currentUser: any;
  publicTest: any;


  constructor(
    private http: HttpClient,
    private socket: Socket
    
  ) {
    console.log("in constructor of manager services");
      //  this.httpOptions = new HttpHeaders({
      //   'Content-Type': 'application/json',
      //       'Accept':'*/*'
      //           });

  }


  // Get history for manager
  getHistoryManager(): Observable<any> {
    
    return this.http.post<any>('http://localhost:8800/manager/allRequestforManager');
  }

  // accept or ignore for manager
  postStatusRequest(requestId,isAccepted): Observable<any> {
 
    return this.http.post<any>('http://localhost:8800/manager/statusOfRequest',
    {requestId,isAccepted});
  }

  fireGetRequestsManager(){
    this.socket.emit('allRequest');
  }
  listenGetRequestsManager():Observable<any>
  {
    return this.socket
        .fromEvent("allRequest");
  }
  
}

