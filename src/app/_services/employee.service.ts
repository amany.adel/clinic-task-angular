import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Socket } from 'ngx-socket-io';
import { AuthGuardGuard } from '../_guards/auth-guard.guard';
import { UserService } from './user.service';
import { Subscription, Observable } from 'rxjs';
import { User } from '../_model/user';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {


  private httpOptions: any;
  currentUser: any;
  publicTest: any;
  currentUserSubscription: Subscription;

  constructor(
    private http: HttpClient,
    private socket: Socket
    // private UserService: UserService
  ) {
    console.log("in constructor of employeee services");
    // if (this.UserService.currentUserValue.token) {
   
    //   console.log('local storage is not null');
    //   this.currentUserSubscription = this.UserService.currentUser.subscribe(user => {
    //     this.currentUser = user;
    //   });
    //   this.httpOptions = {
    //     headers: new HttpHeaders({
    //       'Content-Type': 'application/json',
    //       'Accept': '*/*',
    //       'Authorization': 'Bearer' +this.UserService.currentUserValue.token
    //     })
    //   };
    // }
    // else {
    // console.log('local storga is null');
    //   this.httpOptions = {
    //     headers: new HttpHeaders({
    //       'Content-Type': 'application/json',
    //       'Accept': '*/*'
    //     })
    //   };

    // //   // this.httpOptions = new HttpHeaders({
    // //   //   'Content-Type': 'application/json',
    // //   //       'Accept':'*/*',
    // //   //       'Authorization':`Bearer ${this.currentUser.token}`
    // //   // });

    // }
    
    // this.publicTest = this.httpOptions.headers.Authorization;
  }


  // Get history for employee
  getHistoryEmployee(): Observable<any> {
    
    return this.http.post<any>('http://localhost:8800/employee/allRequest');
  }

   // book Request Employee
   bookRequest(report,dateRequest): Observable<any> {
    
    return this.http.post<any>('http://localhost:8800/employee/book',{report,dateRequest});
  }

  fireGetRequestsEmployee(employeeId){
    this.socket.emit('allRequestForEmployee',employeeId);
  }
  listenGetRequestsEmployee():Observable<any>
  {
    return this.socket
        .fromEvent("allRequestForEmployee");
  }

}
