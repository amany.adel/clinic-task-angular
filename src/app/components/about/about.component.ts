import { Component,  OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { User } from '../../_model/user';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { EmployeeService } from 'src/app/_services/employee.service';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit, OnDestroy {
  currentUser: any;
  currentUserSubscription: Subscription;
  users: User[] = [];
  constructor( private UserService: UserService)  {
    this.currentUserSubscription = this.UserService.currentUser.subscribe(user => {
        this.currentUser = user;
    });
}
ngOnInit() {
 console.log(JSON.stringify(this.currentUser) );
 console.log(this.currentUser.user.type );
 console.log(this.currentUser.token);

//  this.employeeService.getHistoryEmployee(this.currentUser.user._id).subscribe(
//   data => {
//     console.log(data);
//   });


}
ngOnDestroy() {
  // unsubscribe to ensure no memory leaks
  this.currentUserSubscription.unsubscribe();
}

}
