import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { User } from '../../_model/user';
import { Subscription } from 'rxjs';
import { EmployeeService } from '../../_services/employee.service';
import { AlertService } from '../../_services/alert.service';
import { ManagerService } from 'src/app/_services/manager.service';
import { DoctorService } from 'src/app/_services/doctor.service';
import { TouchSequence } from 'selenium-webdriver';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit, OnDestroy {
  doctorReportForm: FormGroup;

  currentUser: any;
  requestData:any;
  currentUserSubscription: Subscription;
  // employeeService:EmployeeService;
  constructor( 
    private UserService: UserService,
    private managerService:ManagerService,
    private doctorService:DoctorService,
    private employeeService:EmployeeService    
    )  
    {
      
      this.currentUserSubscription = this.UserService.currentUser
      .subscribe(user => {
          this.currentUser = user;
      });
  
    }

ngOnInit() {
 console.log(JSON.stringify(this.currentUser) );
 console.log(this.currentUser.user.type );

 if(this.currentUser.user.type=='employee')
 {
  // this.employeeService.getHistoryEmployee().subscribe(data=>{
  //   console.log("data in history "+ JSON.stringify(data));
  //   this.requestData=data.allRequest;
  // });
  this.employeeService.fireGetRequestsEmployee(this.currentUser.user._id);
  this.employeeService.listenGetRequestsEmployee().subscribe(data=>
    {
          console.log("data in history "+ JSON.stringify(data));

      this.requestData=data.allRequest;
    });
 }
 else if (this.currentUser.user.type=='doctor') {
  this.doctorService.fireGetRequestsDoctor();
  this.doctorService.listenGetRequestsDoctor().subscribe(data=>{
      console.log("data in history "+ JSON.stringify(data));
      this.requestData=data.allRequestForDoctor;
    });

 } else {
  // this.managerService.getHistoryManager().subscribe(data=>{
  //   console.log("data in history "+ JSON.stringify(data));
  //   this.requestData=data.allRequest;
  // });
  this.managerService.fireGetRequestsManager();
  this.managerService.listenGetRequestsManager().subscribe(data=>{
      console.log("data in history "+ JSON.stringify(data));
      this.requestData=data.allRequest;
    });
 }
  

}
onClickSubmit(doctorReportForm: NgForm)
{
  console.log(doctorReportForm.value.report);
  console.log(doctorReportForm.value.requestId);
  let report=doctorReportForm.value.report;
  let requestID=doctorReportForm.value.requestId;
  this.doctorService.sendDoctorReport(requestID,report).subscribe(data=>
    {
      
      
      this.doctorService.fireGetRequestsDoctor();
      this.doctorService.listenGetRequestsDoctor().subscribe(data=>{
          console.log("data in history "+ JSON.stringify(data));
          this.requestData=data.allRequestForDoctor;
        });
      
    });

}

onClickEmployeeSubmit(employeeReportForm: NgForm)
{
  console.log(employeeReportForm.value.report);
  console.log(employeeReportForm.value.date);
  let report=employeeReportForm.value.report;
  let date=employeeReportForm.value.date;

  this.employeeService.bookRequest(report,date).subscribe(data=>
    {
      console.log(data);
      this.employeeService.fireGetRequestsEmployee(this.currentUser.user._id);
      this.employeeService.listenGetRequestsEmployee().subscribe(request=>
        {
          this.requestData=request.allRequest;
        });

    });
}
acceptRequest(requestId,status)
{
  console.log("id request is "+requestId+"   staus is "+ status);
  this.managerService.postStatusRequest(requestId,status).subscribe(data=>{
    console.log( JSON.stringify(data));
    this.managerService.fireGetRequestsManager();
  this.managerService.listenGetRequestsManager().subscribe(data=>{
      console.log("data in history "+ JSON.stringify(data));
      this.requestData=data.allRequest;
    });
  });
}

ignoreRequest(requestId,status)
{
  console.log("id request is "+requestId+"   staus is "+ status);
  this.managerService.postStatusRequest(requestId,status).subscribe(data=>{
    console.log( JSON.stringify(data));
  });
}


ngOnDestroy() {
  // unsubscribe to ensure no memory leaks
  this.currentUserSubscription.unsubscribe();
}



}
